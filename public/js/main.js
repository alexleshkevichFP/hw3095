"use strict"

const ajaxSend = async (formData) => {
    const fetchResponse = await fetch('/vote', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: formData
    });

    if (!fetchResponse.ok) {
        throw new Error(`Ошибка ${fetchResponse.status}`);
    }

    return await fetchResponse.json();
};

const fetchStats = async () => {
    const response = await fetch('/stats');
    return response.json();
}

const fetchVotes = async () => {
    const response = await fetch('/variants');
    return response.json();
}

const downloadStats = async (type) => {

    let mime = 'text/html';

    switch (type) {
        case 'json':
            mime = 'application/json';
            break;
        case 'xml':
            mime = 'application/xml';
            break
    }
    const response = await fetch('/get_formatted_data', {
        headers: {
            'Accept': mime
        }
    });

    const data =  response.text();

    data.then((content) => {
        console.log(content);
        document.querySelector('#revealArea').value = content;
    });
}

const parseStatsData = (data) => {
    const stats = data;

    for (let item of stats) {
        const queryElem = document.querySelector(`#stat-${item.id} .count`);

        if (queryElem) {
            queryElem.innerHTML = '';
            queryElem.innerHTML += item.count;
        }
    }
}

document.addEventListener('DOMContentLoaded', () => {

    const form = document.querySelector('#voteForm');

    form.addEventListener('submit', function (e) {
        e.preventDefault();

        const data = JSON.stringify({color: +this.elements['color'].value});

        ajaxSend(data)
            .then((res) => {
                const data = res;
                parseStatsData(data);
            })
    })

    fetchVotes()
        .then((data) => {
            const container = document.querySelector('#voteForm');

            for (let item of data) {
                const template = `
                  <div class="form-check">
                    <input class="form-check-input" type="radio" name="color" id="${item.id}" value="${item.id}" checked>
                    <label class="form-check-label" for="${item.id}">
                        ${item.value}
                    </label>
                  </div>
                `;

                container.innerHTML += template;
            }

            container.innerHTML += `<hr><button class="btn btn-primary">Submit</button>`;
        });

    fetchStats()
        .then((data) => {
            parseStatsData(data);
        })
})




