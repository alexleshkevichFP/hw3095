const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const fs = require('fs');

const { body, check, validationResult } = require('express-validator');

const app = express();

app.use(bodyParser.json());

app.use(express.static(path.join(__dirname, 'public')));

function readFile(name) {
    let fileLocation = path.join(__dirname, 'data', name);

    return new Promise((resolve, reject) => {

        fs.readFile(fileLocation, 'utf8', (err, content) => {
            if (err) {
                console.log(err);
                return;
            }

            try {
                const data = JSON.parse(content);
                resolve(data);
            } catch (err) {
                reject(err);
            }
        })
    })
}

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'views', 'index.html'))
});

app.get('/stats', (req, res) => {
    readFile('stats.json')
        .then((data) => {
            res.status(200).json(data);
        })
});

app.get('/variants', (req, res) => {
    readFile('votes.json')
        .then((data) => {
            res.status(200).json(data);
        });
});

app.post('/vote', [
    check('color')
        .isNumeric()
], (req, res) => {

    const errors = validationResult(req)

    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() })
    }

    readFile('stats.json')
        .then((data) => {
            const colorId = req.body.color;
            const statsData = data;

            const selectedColor = statsData.find(item => item.id === parseInt(colorId))

            if (selectedColor) {
                selectedColor.count += 1;
            }

            const fileData = JSON.stringify(statsData);
            const filePath = path.join(__dirname, 'data', 'stats.json');

            fs.writeFile(filePath, fileData, function (err,fileData) {
                if (err) {
                    return console.log(err);
                }

                readFile('stats.json')
                    .then((data) => {
                        res.status(200).json(data)
                    })
            });
        });
});

app.get('/get_formatted_data', (req, res) => {


    readFile('stats.json')
        .then((data) => {

            console.log(data);
            let template;

            if (req.accepts('html')) {
                console.log('html');
                res.setHeader('Content-type', 'text/html');
                template = `
                      <div id="stat-0">
                        <strong>RED:</strong> <span class="count">${data[0].count}</span>
                    </div>
                    <div id="stat-1">
                        <strong>GREEN:</strong> <span class="count">${data[1].count}</span>
                    </div>
                    <div id="stat-2">
                        <strong>BLUE:</strong> <span class="count">${data[2].count}</span>
                    </div>
                `;
            }
            else if (req.accepts('xml')) {
                console.log('xml');
                res.setHeader('Content-type', 'application/xml');
                template = `
                <?xml version="1.0" encoding="UTF-8"?>
                    <root>
                       <element>
                          <count>${data[0].count}</count>
                       </element>
                       <element>
                          <count>${data[1].count}</count>
                       </element>
                       <element>
                          <count>${data[2].count}</count>
                       </element>
                    </root>
                `;
            }
            else if (req.accepts('json')) {
                res.setHeader('Content-type', 'application/json');
                template = JSON.stringify(data);
            }

            res.status(200).send(template)
        });
});

app.listen(7480);
